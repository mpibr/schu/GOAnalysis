# GOAnalysis
A perl script to perform gene ontology analysis of two unrancked lists.

## Dependences

GOAnalysis requires the [Math::GSL::CDF](http://search.cpan.org/~leto/Math-GSL-0.39/lib/Math/GSL/CDF.pm) module to be installed, which has a dependency to the [GNU Scientific Library (GSL)](https://www.gnu.org/software/gsl/).

First, you will have to install [GSL](https://www.gnu.org/software/gsl/) on your system:

* On Debian/Ubuntu you can use:

```
sudo apt-get install libgsl0-dev
```

* On Mac you can use homebrew (http://brew.sh/):

```
brew install gsl
```

* On Fedora/CentOS/RedHat/openSUSE you can use

```
sudo yum install gsl-devel
```

* Or get GSL at http://www.gnu.org/software/gsl


After that, install the Perl module from CPAN like this:

``` 
cpan Math::GSL::CDF
```
*Note: you might need to install it with `sudo`*
