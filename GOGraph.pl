#!/usr/bin/perl

use warnings;
use strict;

my $obo_file = shift;


my $id;
my $name;
my $namespace;

my %gograph = ();


# parse obo file
open (my $fh, "<", $obo_file) or die $!;
while (my $line = <$fh>)
{
    chomp($line);
    
    if ($line eq '[Term]')
    {
        $id = <$fh> =~ m/id\:\s(GO\:[0-9]+)/ ? $1 : '';
        $name = <$fh> =~ m/name\:\s(.+)$/  ? $1 : '';
        $namespace = <$fh> =~ m/namespace\:\s(.+)$/  ? $1 : '';
        
        $gograph{$id}{"name"} = $name;
        $gograph{$id}{"namespace"} = $namespace;
        
    }
    elsif (($line =~ m/^is_a\:/) || ($line =~ /^relationship\:/))
    {
        # is_a
        
        # relationship
        ### part_of
        ### negatively_regulates
        ### positively_regulates
        ### regulates
        
        my $parent_id = $line =~ m/.+(GO\:[0-9]+)/ ? $1 : '';
        
        if (defined($id) && defined($parent_id))
        {
            $gograph{$id}{"parents"}{$parent_id}++;
        }
        
    }
    elsif ($line eq '')
    {
        $id = '';
        $name = '';
        $namespace = '';
        #print $id,"\t",$name,"\t",$namespace,"\n" if(defined($id));
    }
    
    
    
}

foreach my $term (keys %gograph)
{
    print $term,"\t",scalar(keys %{$gograph{$term}{"parents"}}),"\n";
}


